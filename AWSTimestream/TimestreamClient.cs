﻿using Amazon.TimestreamQuery;
using Amazon.TimestreamQuery.Model;
using Amazon.TimestreamWrite;
using Amazon.TimestreamWrite.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IgnitionTelemetry.AWSTimestream
{
    public class TimestreamClient
    {
        private AmazonTimestreamWriteClient writeClient;
        private AmazonTimestreamQueryClient queryClient;

        public TimestreamClient()
        {
            var queryClientConfig = new AmazonTimestreamQueryConfig
            {
                Timeout = TimeSpan.FromSeconds(20),
                MaxErrorRetry = 10
            };
            queryClient = new AmazonTimestreamQueryClient(queryClientConfig);

            var writeClientConfig = new AmazonTimestreamWriteConfig
            {
                Timeout = TimeSpan.FromSeconds(20),
                MaxErrorRetry = 10
            };
            writeClient = new AmazonTimestreamWriteClient(writeClientConfig);
        }
        public async Task<DateTime?> GetLastTimestamp(string database, string table, string host)
        {
            DateTime? lastTimestamp = null;

            string query = $"select max(time) from \"{database}\".\"{table}\" where host = '{host}'";
           
            QueryRequest queryRequest = new QueryRequest();
            queryRequest.QueryString = query;
            QueryResponse queryResponse = await queryClient.QueryAsync(queryRequest);

            string timeStamp = queryResponse.Rows[0].Data[0].ScalarValue;
            if (string.IsNullOrEmpty(timeStamp))
            {
                return null;
            }
            lastTimestamp = Convert.ToDateTime(timeStamp);
            return lastTimestamp;
        }

        public async Task WriteRecords(string databaseName, string tableName, List<Record> records)
        {                    
            var writeRecordsRequest = new WriteRecordsRequest
            {
                DatabaseName = databaseName,
                TableName = tableName,
                Records = records
            };
            WriteRecordsResponse response = await writeClient.WriteRecordsAsync(writeRecordsRequest);                                        
        }
    }
   
}
