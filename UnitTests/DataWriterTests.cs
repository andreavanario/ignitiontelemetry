﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IgnitionTelemetry;
using IgnitionTelemetry.DataWriters;


namespace UnitTests
{
    [TestClass]
    public class DataWriterTests
    {
        [TestMethod]
        public async Task MsSqlIgnitionAlarmDataWriterHarvestData()
        {
            AppTask appTask = new AppTask()
            {
                Title = "APS IGNITION Alarm Event extraction",
                TargetDb = "APS-test",
                DataSource = "@\"Data Source=DESKTOP-SJS3EA3\\SQLEXPRESS;Initial Catalog=LocalHistoryDB;User ID=sa;Password=c@v0t3c\"",
                TargetTable = "test-table",
                BatchSize = 10,
            };

            MsSqlIgnitionAlarmEventDataWriter edw = new MsSqlIgnitionAlarmEventDataWriter(appTask, "DESKTOP-SJS3EA3");
            DateTime dt = new DateTime(2021, 9, 1);
            var data = await edw.HarvestDataFromSource(dt);
            Assert.IsNotNull(data);
            Assert.IsTrue(data.Count > 0);
        }
    }
}
