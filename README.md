
## Ignition Telemetry
Ignition Telemetry is an application designed to harvest data from various data sources, and publish it to AWS Timestream. It is a self-contained .Net Core console application that can be run from the command line on any Windows 10 or Linux host. 

Its operates like s task runner. A collection of data harvesting tasks are defined in the JSON configuration file. The application executes each of these tasks in sequence, harvesting data from a defined data source, and writing the data to a defined database/table in AWS Timestream.
## Dependencies
You will need .Net Core framework version 3.1 on you machine.
Due to this dependency, this application is not suited to Windows 7 hosts.
## AWS Credentials
For the app to access Timestream, you will need to download an AWS access key. 
See https://docs.aws.amazon.com/timestream/latest/developerguide/accessing.html for details about how to do this.
The keys usually get stored in a \\.aws folder in the same place as the executable.
## Configuration
The app uses a JSON configuration file called `appconfig.json`. This is a required file, without it the app does not know what to do.
The most important part of the configuration file is the tasks collection. This describes the datasources and associated data processing class to use to harvest data and publish it to Timestream.
## Build and run
After you have cloned the repo to your machine, use following commands to build and run the app.
**Don't worry if see a bunch of compiler warnings about `async methods lacking await operators`, this is .Net being anal.**

`> dotnet build`

`> dotnet run --project ignitiontelemetry`
## How to extend the project to add your own data harvesting tasks
Its quite simple to add additional tasks.

1. Add a new C# class to the IgnitionTelemetry project. Make sure you give your class a descriptive name.

2. Make your new class inherit from the abstract class `CloudDataWriter`, which knows how to write data to AWS Timestream.

3. Override the virtual method `HarvestDataFromSource()`. This is where you put your custom code, which generally follows the following pattern:

    * establish connection to your datasource

    * query datasource for all new data since last timestamp

    * parse the data into records which the native format for ingesting data into AWS Timestream

    * return your list of records at the end of `HarvestDataFromSource()` which will then write it into AWS Timestream

4. Add your class definition to the `switch` statement of the `MainAsync()` method in `Program.cs`

5. Add your your task to the `appconfig.js` file

## Todos and possible improvements
* Put the configuration into a cloud database instead of a json file, and make the app retrieve it every time it runs.
* Change the mechanism for instantiating the DataWriter classes to be dynamic so that new classes could be added to the app simply by adding a new assembly to the \\bin folder.
* More Unit tests !!!
* Make use of dependency injection to improve unit testing