﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using Amazon.TimestreamWrite;
using Amazon.TimestreamWrite.Model;
using Serilog;

namespace IgnitionTelemetry.DataWriters
{
    public class EdgeAlarmEventDataWriter:CloudDataWriter
    {
        const string AlarmEventQuery = "select source, displaypath, priority, eventtype, eventflags, eventtime from alarm_events where eventtime > {0} order by eventtime limit {1}";

        public EdgeAlarmEventDataWriter(AppTask task, string hostName) : base()
        {
            connectionString = task.DataSource;
            databaseName = task.TargetDb;
            tableName = task.TargetTable;
            batchSize = task.BatchSize;
            host = hostName;
            Log.Information($"Starting Alarm Event data harvest for {host}...");
        }
        public override async Task<List<Record>> HarvestDataFromSource(DateTime dtLastTimeStamp)
        {
            var records = new List<Record>();
            long lastTimeStamp = Utils.ConvertToUnixTimestamp(dtLastTimeStamp);
            string query = string.Format(AlarmEventQuery, lastTimeStamp, batchSize);
            Log.Debug(query);
            try
            {
                using (var connection = new SQLiteConnection(connectionString))
                {
                    var command = connection.CreateCommand();
                    command.CommandText = query;
                    connection.Open();
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        string source = reader.GetString(0);
                        string alarmName = ParseAlarmName(source);
                        if (string.IsNullOrEmpty(alarmName))
                        {
                            alarmName = source.Replace(":", "");
                        }
                        string displayName = reader.GetString(1);
                        int priority = reader.GetInt32(2);
                        int eventType = reader.GetInt32(3);
                        int eventFlags = reader.GetInt32(4);
                        long eventTime = reader.GetInt64(5);

                        string alarmPriority = string.Empty;
                        switch (priority)
                        {
                            case 0:
                                alarmPriority = "Diagnostic";
                                break;
                            case 1:
                                alarmPriority = "Low";
                                break;
                            case 2:
                                alarmPriority = "Medium";
                                break;
                            case 3:
                                alarmPriority = "High";
                                break;
                            case 4:
                                alarmPriority = "Critical";
                                break;
                        }

                        string alarmType = string.Empty;
                        switch (eventType)
                        {
                            case 0:
                                alarmType = "Active";
                                break;
                            case 1:
                                alarmType = "Clear";
                                break;
                            case 2:
                                alarmType = "Ack";
                                break;
                        }

                        Log.Debug(string.Format("{0}, {1}, {2}, {3}, {4}, {5} ",
                            source,
                            displayName,
                            alarmName,
                            alarmType,
                            alarmPriority,
                            eventTime
                        ));

                        List<Dimension> dimensions = new List<Dimension>{
                            new Dimension { Name = "source", Value = source },
                            new Dimension { Name = "priority", Value = alarmPriority },
                            new Dimension { Name = "eventflags", Value = eventFlags.ToString() },
                            new Dimension { Name = "host", Value = host }
                        };

                        if (!string.IsNullOrEmpty(displayName))
                        {
                            dimensions.Add(new Dimension { Name = "displayname", Value = displayName });
                        }

                        var tagValueRecord = new Record
                        {
                            Dimensions = dimensions,
                            MeasureName = alarmName,
                            MeasureValue = alarmType,
                            MeasureValueType = MeasureValueType.VARCHAR,
                            Time = eventTime.ToString()
                        };

                        records.Add(tagValueRecord);
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return records;
        }
        public string ParseAlarmName(string source)
        {
            string name = String.Empty;
            int i = source.IndexOf(":/alm:");
            if (i > 0)
            {
                name = source.Substring(i + 6, source.Length - (i + 6));
            }
            return name;
        }
    }
}
