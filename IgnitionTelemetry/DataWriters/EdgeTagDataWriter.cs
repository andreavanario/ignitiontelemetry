﻿using Amazon.TimestreamWrite;
using Amazon.TimestreamWrite.Model;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;
using Serilog;

namespace IgnitionTelemetry.DataWriters
{
    public class EdgeTagDataWriter:CloudDataWriter
    {
        const string TagDataQuery = "select tagpath, numvalue, strvalue, t_stamp from tags t inner join tagdata td on t.id = td.tagid where t_stamp > {0} order by t_stamp limit {1}";
        public EdgeTagDataWriter(AppTask task, string hostName) : base()
        {
            connectionString = task.DataSource;
            databaseName = task.TargetDb;
            tableName = task.TargetTable;
            batchSize = task.BatchSize;
            host = hostName;
            Log.Information($"Starting BDU tag data harvest for {host}...");
        }
        public override async Task<List<Record>> HarvestDataFromSource(DateTime dtLastTimeStamp)
        {
            var records = new List<Record>();
            long lastTimeStamp = Utils.ConvertToUnixTimestamp((DateTime)dtLastTimeStamp);
            string query = string.Format(TagDataQuery, lastTimeStamp, batchSize);
            Log.Debug(query);
            try
            {
                using (var connection = new SQLiteConnection(connectionString))
                {
                    var command = connection.CreateCommand();
                    command.CommandText = query;
                    connection.Open();
                    var reader = await command.ExecuteReaderAsync();
                    while (reader.Read())
                    {
                        string tag = reader.GetString(0);
                        double? numvalue = reader.GetDouble(1);
                        string strvalue = reader.IsDBNull(2) ? String.Empty : reader.GetString(2);
                        long timeStamp = reader.GetInt64(3);

                        MeasureValueType valueType = MeasureValueType.VARCHAR;
                        string val = strvalue;
                        if (numvalue != null)
                        {
                            val = numvalue.ToString();
                            valueType = MeasureValueType.DOUBLE;
                        }

                        Log.Debug(string.Format("{0}: {1} {2}", tag, val, timeStamp));

                        List<Dimension> dimensions = new List<Dimension>{
                            new Dimension { Name = "host", Value = host }
                        };

                        var tagValueRecord = new Record
                        {
                            Dimensions = dimensions,
                            MeasureName = tag,
                            MeasureValue = val,
                            MeasureValueType = valueType,
                            Time = timeStamp.ToString()
                        };

                        records.Add(tagValueRecord);
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return records;
        }           
    }
}
