﻿using Amazon.TimestreamWrite.Model;
using Amazon.TimestreamWrite;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data;

namespace IgnitionTelemetry.DataWriters
{
    public class MsSqlIgnitionTagDataWriter : CloudDataWriter
    {

        const string TagDataQuery =
            @"SELECT * FROM
                (SELECT * FROM sqlt_data_1_2021_09 
                union all
                SELECT * FROM sqlt_data_1_2021_10) AS result
            WHERE result.t_stamp > {1}
            order by t_stamp LIMIT {2}";

        const string sSourceTableQuery = "select pname from sqlth_partitions where start_time <  {0} and end_time >= {0};";

        const string Last2TablesQuery = @"select top 2 schema_name(schema_id) as schema_name,
                                                name as table_name,
                                                create_date,
                                                modify_date
                                                from sys.tables
                                                where name like '%sqlt_data%'  
                                                order by create_date desc;";


        public MsSqlIgnitionTagDataWriter(AppTask task, string hostName) : base()
        {
            connectionString = task.DataSource;
            databaseName = task.TargetDb;
            tableName = task.TargetTable;
            batchSize = task.BatchSize;
            host = hostName;
            Log.Information($"Starting MMU tag data harvest for {host}...");
        }
        public override async Task<List<Record>> HarvestDataFromSource(DateTime dtLastTimeStamp)
        {
            var records = new List<Record>();
            long lastTimeStamp = Utils.ConvertToUnixTimestamp((DateTime)dtLastTimeStamp);
            string []sourceTable = GetSourceTable(lastTimeStamp);
            if (string.IsNullOrEmpty(sourceTable[0]))
            {
                Log.Warning("Could not find any historical tag data tables for the timestamp {0}", lastTimeStamp);
                return records;
            }
            string query = string.Format(TagDataQuery, sourceTable, lastTimeStamp, batchSize);
            Log.Debug(query);
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    var command = connection.CreateCommand();
                    command.CommandText = query;
                    connection.Open();
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        string tagpath = reader.GetString(0);
                        Int64? intValue = null;
                        if (!reader.IsDBNull(1))
                        {
                            intValue = reader.GetInt64(1);
                        }

                        double? floatValue = null;
                        if (!reader.IsDBNull(2))
                        {
                            floatValue = reader.GetDouble(2);
                        }

                        string stringValue = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);

                        DateTime? dateValue = null;
                        if (!reader.IsDBNull(4))
                        {
                            //SqlDateTime dv = reader.GetMySqlDateTime(4);
                            //dateValue = (DateTime)dv; //new DateTime(dv.Year,dv.Month,dv.Day,dv.Hour,dv.Minute,dv.Second);
                        }

                        long timeStamp = reader.GetInt64(5);

                        Console.WriteLine(string.Format("{0}, {1}, {2}, {3}, {4}, {5} ",
                            tagpath,
                            intValue,
                            floatValue,
                            stringValue,
                            dateValue.ToString(),
                            timeStamp.ToString()
                        ));
                        string val = stringValue;
                        MeasureValueType valueType = MeasureValueType.VARCHAR;

                        if (intValue != null)
                        {
                            val = intValue.ToString();
                            valueType = MeasureValueType.BIGINT;
                        }
                        if (floatValue != null)
                        {
                            val = floatValue.ToString();
                            valueType = MeasureValueType.DOUBLE;
                        }
                        if (dateValue != null)
                        {
                            val = dateValue.ToString();
                            valueType = MeasureValueType.VARCHAR;
                        }

                        // record must have a value otherwise Timestream will reject it
                        if (!string.IsNullOrEmpty(val))
                        {
                            List<Dimension> dimensions = new List<Dimension>{
                            new Dimension { Name = "host", Value = host }
                        };

                            var tagValueRecord = new Record
                            {
                                Dimensions = dimensions,
                                MeasureName = tagpath,
                                MeasureValue = val,
                                MeasureValueType = valueType,
                                Time = timeStamp.ToString()
                            };

                            records.Add(tagValueRecord);
                        }

                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return records;
        }




        public string[] GetSourceTable(long lastSample)
        {
            string sourceTable = string.Empty;
            string sourceTableQuery = string.Format(sSourceTableQuery, lastSample);
            string[] tablesResult= new string[2];            
            try {

                using (var connection = new SqlConnection(connectionString))
                {
                    var cmdSelectTables = new SqlCommand(Last2TablesQuery, connection);
                    var cmdQuery = new SqlCommand(sourceTableQuery, connection);

                    connection.Open();
                    //get the last tables name to interrogate
                    SqlDataReader dr1 = cmdSelectTables.ExecuteReader();
                    DataTable schemaTable = dr1.GetSchemaTable();

                    foreach (DataRow row in schemaTable.Rows)
                    {
                        foreach (DataColumn column in schemaTable.Columns)
                        {
                            Console.WriteLine(String.Format("{0} = {1}", column.ColumnName, row[column]));                
                        }
                    }

                    tablesResult[0].Insert(0, schemaTable.Rows[1][2].ToString());
                    tablesResult[1].Insert(1, schemaTable.Rows[1][2].ToString());

                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return tablesResult;
        }
    }
}
