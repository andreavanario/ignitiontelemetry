﻿using Amazon.TimestreamWrite.Model;
using IgnitionTelemetry.AWSTimestream;
using System;
using System.Collections.Generic;
using Serilog;
using System.Threading.Tasks;

namespace IgnitionTelemetry.DataWriters
{
    /*********************************************************************************
      The CloudDataWriter abstract class implements the code for writing
      data to AWS Timestream in the form of a List of Records.
      
      To use it:
         1. Create a new class that inherites from this one
         2. Override the HarvestDataFromSource() method with your custom code
         3. return your list of records once you have assembled them and let 
            the CloudDataWriter do the rest.
         4. Add your class to the switch block in Program.cs
         5. Add your task configuration to the appconfig.json file
      
    *********************************************************************************/
    public abstract class CloudDataWriter
    {
        protected string connectionString;
        protected TimestreamClient client = new TimestreamClient();

        protected string databaseName;// = "Ignition";
        protected string tableName;// = "tag-data";
        protected string host;
        protected int batchSize;

        public CloudDataWriter()
        {            
            client = new TimestreamClient();
        }
        public virtual async Task<int> WriteData()
        {
            int numRecords = 0;
            try
            {
                // get most recent timestamp from Timestream
                DateTime? dt = await client.GetLastTimestamp(databaseName, tableName, host);

                //DateTime? dt = null;//= DateTime.Now.AddMinutes(-120);
                //dt = dt.AddHours(-2); //timezone

                if (dt == null)
                {
                    Log.Warning($"No data found in {databaseName}.{tableName} for '{host}'. Assuming start date 1 week prior to now.");
                    dt = DateTime.Now.AddDays(-7);                    
                }
                DateTime dtLastTimeStamp = (DateTime)dt;

                var records = await HarvestDataFromSource(dtLastTimeStamp);
                if (records.Count > 0)
                {
                    await client.WriteRecords(databaseName, tableName, records);
                    Log.Information($"Successfully written {records.Count} records to cloud.");
                }
                else
                {
                    Log.Information("No new records found.");
                }
                numRecords = records.Count;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
            return numRecords;
        }

        // Override this method and add your custom code
        public abstract Task<List<Record>> HarvestDataFromSource(DateTime dtLastTimeStamp);

    }
}
