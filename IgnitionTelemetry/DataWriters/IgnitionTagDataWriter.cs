﻿using Amazon.TimestreamWrite;
using Amazon.TimestreamWrite.Model;
using MySqlConnector;
using System;
using System.Collections.Generic;
using Serilog;
using System.Threading.Tasks;

namespace IgnitionTelemetry.DataWriters
{
    public class IgnitionTagDataWriter:CloudDataWriter
    {
        const string TagDataQuery = @"select t.tagpath, h.intvalue, h.floatvalue, h.stringvalue, h.datevalue, t_stamp 
            from sqlth_te t
            inner join {0} h on t.id = h.tagid
            where t_stamp > {1}
            order by t_stamp limit {2}";
        const string sSourceTableQuery = "select pname from sqlth_partitions where start_time <  {0} and end_time >= {0};";

        public IgnitionTagDataWriter(AppTask task, string hostName) : base()
        {
            connectionString = task.DataSource;
            databaseName = task.TargetDb;
            tableName = task.TargetTable;
            batchSize = task.BatchSize;
            host = hostName;
            Log.Information($"Starting MMU tag data harvest for {host}...");
        }
        public override async Task<List<Record>> HarvestDataFromSource(DateTime dtLastTimeStamp)
        {
            var records = new List<Record>();
            long lastTimeStamp = Utils.ConvertToUnixTimestamp((DateTime)dtLastTimeStamp);
            string sourceTable = GetSourceTable(lastTimeStamp);
            if (string.IsNullOrEmpty(sourceTable))
            {
                Log.Warning("Could not find any historical tag data tables for the timestamp {0}", lastTimeStamp);
                return records;
            }
            string query = string.Format(TagDataQuery, sourceTable, lastTimeStamp, batchSize);
            Log.Debug(query);
            try
            {
                using (var connection = new MySqlConnection(connectionString))
                {
                    var command = connection.CreateCommand();
                    command.CommandText = query;
                    connection.Open();
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        string tagpath = reader.GetString(0);
                        Int64? intValue = null;
                        if (!reader.IsDBNull(1))
                        {
                            intValue = reader.GetInt64(1);
                        }

                        double? floatValue = null;
                        if (!reader.IsDBNull(2))
                        {
                            floatValue = reader.GetDouble(2);
                        }

                        string stringValue = reader.IsDBNull(3) ? string.Empty : reader.GetString(3);

                        DateTime? dateValue = null;
                        if (!reader.IsDBNull(4))
                        {
                            MySqlDateTime dv = reader.GetMySqlDateTime(4);
                            dateValue = (DateTime)dv; //new DateTime(dv.Year,dv.Month,dv.Day,dv.Hour,dv.Minute,dv.Second);
                        }

                        long timeStamp = reader.GetInt64(5);

                        Console.WriteLine(string.Format("{0}, {1}, {2}, {3}, {4}, {5} ",
                            tagpath,
                            intValue,
                            floatValue,
                            stringValue,
                            dateValue.ToString(),
                            timeStamp.ToString()
                        ));
                        string val = stringValue;
                        MeasureValueType valueType = MeasureValueType.VARCHAR;

                        if (intValue != null)
                        {
                            val = intValue.ToString();
                            valueType = MeasureValueType.BIGINT;
                        }
                        if (floatValue != null)
                        {
                            val = floatValue.ToString();
                            valueType = MeasureValueType.DOUBLE;
                        }
                        if (dateValue != null)
                        {
                            val = dateValue.ToString();
                            valueType = MeasureValueType.VARCHAR;
                        }

                        // record must have a value otherwise Timestream will reject it
                        if (!string.IsNullOrEmpty(val))
                        {
                            List<Dimension> dimensions = new List<Dimension>{
                            new Dimension { Name = "host", Value = host }
                        };

                            var tagValueRecord = new Record
                            {
                                Dimensions = dimensions,
                                MeasureName = tagpath,
                                MeasureValue = val,
                                MeasureValueType = valueType,
                                Time = timeStamp.ToString()
                            };

                            records.Add(tagValueRecord);
                        }

                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return records;
        }
        public string GetSourceTable(long lastSample)
        {
            string sourceTable = string.Empty;
            string sourceTableQuery = string.Format(sSourceTableQuery, lastSample);
            using (var connection = new MySqlConnection(connectionString))
            {
                var command = new MySqlCommand(sourceTableQuery, connection);
                connection.Open();
                var reader = command.ExecuteReader();
                if (reader.Read())
                {
                    sourceTable = reader.GetString(0);
                }
                connection.Close();
            }
            return sourceTable;
        }
    }
}
