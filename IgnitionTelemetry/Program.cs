﻿using System;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using IgnitionTelemetry.DataWriters;
using CommandLine;
using Microsoft.Extensions.Configuration;
using Serilog;


namespace IgnitionTelemetry
{
    public class Program
    {        
        static void Main(string[] args)
        {           
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appconfig.json", false, true)
                .Build();

            var config = configuration.Get<AppConfig>();

            ConfigureLogging(config);
            
            MainAsync(config).GetAwaiter().GetResult();            
        }
        static async Task MainAsync(AppConfig config)
        {
            string version = GetAssemblyVersion();
            Log.Information($"IgnitionTelemetry v{version} starting data harvesting tasks...");

            foreach(var task in config.Tasks.OrderBy(t=> t.Sequence))
            {
                switch(task.TaskType)
                {
                    case "IgnitionAlarmEventDataWriter":
                        IgnitionAlarmEventDataWriter adw = new IgnitionAlarmEventDataWriter(task, config.Host);
                        await adw.WriteData();
                        Log.Information($"adw");
                        break;
                    case "IgnitionTagDataWriter":
                        IgnitionTagDataWriter mtw  = new IgnitionTagDataWriter(task, config.Host);
                        await mtw.WriteData();
                        Log.Information($"mtw");
                        break;
                    case "EdgeTagDataWriter":
                        EdgeTagDataWriter bdw = new EdgeTagDataWriter(task, config.Host);
                        await bdw.WriteData();
                        Log.Information($"bdw");
                        break;
                    case "EdgeAlarmEventDataWriter":
                        EdgeAlarmEventDataWriter eaw = new EdgeAlarmEventDataWriter(task, config.Host);
                        await eaw.WriteData();
                        Log.Information($"eaw");
                        break;
                    case "MikrotikDataWriter":
                    case "MsSqlIgnitionAlarmEventDataWriter":
                        MsSqlIgnitionAlarmEventDataWriter miaw = new MsSqlIgnitionAlarmEventDataWriter(task, config.Host);
                        await miaw.WriteData();
                        Log.Information($"mssql_iadw");
                        break;
                    case "CradlepointDataWriter":                        
                    default:
                        Console.WriteLine(string.Format("TaskType '{0}' not implemented", task.TaskType));
                        break;
                }
            }

            Log.Information("IgnitionTelemetry finished.");                                           
        }        
        static void ConfigureLogging(AppConfig config)
        {
            if (config.LogLevel.ToLower() == "debug")
            {
                Log.Logger = new LoggerConfiguration()
                   .Enrich.FromLogContext()
                   .MinimumLevel.Debug()
                   .WriteTo.Console()
                   .WriteTo.File(config.LogFile, rollOnFileSizeLimit: true, fileSizeLimitBytes: 1000000)
                   .CreateLogger();
            }
            else
            {
                Log.Logger = new LoggerConfiguration()
                  .Enrich.FromLogContext()
                  .MinimumLevel.Information()
                  .WriteTo.Console()
                  .WriteTo.File(config.LogFile, rollOnFileSizeLimit: true, fileSizeLimitBytes: 1000000)
                  .CreateLogger();
            }
        }
        static string GetAssemblyVersion()
        {
            var version = Assembly.GetEntryAssembly().GetName().Version;
            return $"{version.Major}.{version.Minor}.{version.Build}";
        }
    }
   
    public class Options
    {
        [Option('l', "list-tags", Required = false, HelpText = "List all available tags in target database")]
        public bool ListTags { get; set; }

        [Option('a', "list-alarms", Required = false, HelpText = "List most recent alarm events in target database")]
        public bool ListAlarms { get; set; }

        [Option('t', "test-aws", Required = false, HelpText = "Test cloud connection to AWS Timestream")]
        public bool TestAWS { get; set; }
    }
}
