﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace IgnitionTelemetry
{
    public static class Utils
    {
        public static async Task<long> GetLastQuery(string lastSampleFilename)
        {
            string text = await File.ReadAllTextAsync(lastSampleFilename);
            return Convert.ToInt64(text);
        }
        public static async Task SaveLastQuery(long lastQuery, string lastSampleFilename)
        {
            await File.WriteAllTextAsync(lastSampleFilename, lastQuery.ToString());
        }
        public static DateTime ConvertUnixTimestampToUtc(long unixTimeStamp)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            DateTime dt = epoch.AddMilliseconds(unixTimeStamp);
            return dt;
        }
        public static long ConvertToUnixTimestamp(DateTime dt)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan ts = dt - epoch;
            return (long)ts.TotalMilliseconds;
        }
    }
}
