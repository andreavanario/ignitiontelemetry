﻿-------------------------------------------------
IgnitionTelemetry
damien.tiede@cavotec.com Sept 2021
-------------------------------------------------

The purpose of this application is to harvest tag data from an Ignition tag historian database and write it to AWS Timestream database.

