﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IgnitionTelemetry
{
    public class AppConfig
    {
        public string Host { get; set; }
        public string LogLevel { get; set; }
        public string LogFile { get; set; }
        public List<AppTask> Tasks { get; set; }        
    }
    public class AppTask
    {
        public int Sequence { get; set; }
        public string Title { get; set; }
        public string TaskType { get; set; }
        public string DataSource { get; set; }
        public string TargetDb { get; set; }
        public string TargetTable { get; set; }
        public int BatchSize { get; set; }
    }
}
